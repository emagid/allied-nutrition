<?php get_header(); ?>
<div id="heroImage" style="background-image:url('<?php the_field('main_banner'); ?>')">
        <div class="overlay_hero">

            <h1><?php the_field('welcome_title'); ?></h1>
    </div>

</div>

 	<div id="main">
 		<div id="welcomeSection">
 			
 			<?php the_field('welcome_text'); ?>
 		</div>
 		<div id="doctor"></div>

 		<div class="doctor-wrapper">
            <div class="meetSection" id="meetTheDoctor">
                <h1 id="meet_docs">Your Nutrition & Wellness Team</h1>
 			<?php
	  			$args = array(
	    		'post_type' => 'meetourdoctors',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

		
	 		<div id="meetText">
                <img src="<?php the_field('image'); ?>" />
	 			<h1><?php the_field('doctor'); ?></h1>
				<p><?php the_field('text'); ?></p>
	 		</div>
		

		<?php
			}
				}
			else {
			echo 'No Doctors Found';
			}
		?>
                </div>
		</div>
        
                <div id="testimonial"></div>
        <div class="feedback_section">
                    <h1>Testimonials</h1>
            
            <div class="feedback_scroller">
                
                 			<?php
	  			$args = array(
	    		'post_type' => 'testimonials',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
                <div>
                    <p><?php the_field('review'); ?></p>
                </div>
                
                		<?php
			}
				}
			else {
			echo 'No Reviews Found';
			}
		?>

                </div>
        
        </div>
        
        
        
		<div id="policy"></div>
		<div id="policySection">
			<h1>We are here to help your family with:</h1>
				 	<?$id = 253;
				 	$post = get_post($id); ?>
				<p><?php the_field('office_policies'); ?></p>


 		</div>
 		<div id="hours"></div>
 		 	<?php
	  			$args = array(
	    		'post_type' => 'office-hours',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
 		<div id="hoursSection">
 			<h2><?php the_field('office_name'); ?></h2>
 			<p><?php the_field('comments'); ?></p>
 			<div id="hoursIcon">
 				<img src="<?php bloginfo('template_directory'); ?>/images/clock-icon.png">
 				<h1>Hours</h1>	
 			</div>

 			<div id="hoursSchedule">



 				<table>
 
				  <tr>
				    <td>Monday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('monday'); ?></td>
				  </tr>

				  <tr>
				    <td>Tuesday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('tuesday'); ?></td>
				  </tr>

				  <tr>
				    <td>Wednesday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('wednesday'); ?></td>
				  </tr>

				  <tr>
				    <td>Thursday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('thursday'); ?></td>
				  </tr>

				  <tr>
				    <td>Friday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('friday'); ?></td>
				  </tr>

				  <tr>
				    <td>Saturday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('saturday'); ?></td>
				  </tr>

				  <tr>
				    <td>Sunday</td>
				    <td><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon.png"></td> 
				    <td class="timeSlot"><?php the_field('sunday'); ?></td>
				  </tr>

				</table>

 			</div> <!-- hoursSchedule -->
 			<p><?php the_field('sub_comments'); ?></p>
 			</div> <!-- hoursSection -->
			<?php
				}
					}
				else {
				echo 'No Text Found';
				}
			?>


 			<div id="plansSection">
 				<h1>Insurance Plans</h1>
 				<div id="plansProviders">
 			<?php
	  			$args = array(
	    		'post_type' => 'insurance-plans',
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
				<?php the_field('insurance_content'); ?>
 			<?php
				}
					}
				else {
				echo 'No Plans Found';
				}
			?>	
 				</div>	
 			</div>

 			<div id="contact"></div>
 			<div id="contactSection">

 				<h1>Contact Us</h1>
 				<h6 class="email-link">
 					</h6>
 				<div class="contactDivs">
	 				<div class="contactBlock">
	 					<?$id = 42;
						$post = get_post($id); 
						$content = $post->post_content;
						?>
	 					<img src="<?php bloginfo('template_directory'); ?>/images/pinpoint-icon.png">
	 					
	 					<?=$content?>

	 				</div>
	 				<div class="contactBlock contactPhone">
	 					<?$id = 45;
						$post = get_post($id); 
						$content = $post->post_content;
						?>
	 					<img src="<?php bloginfo('template_directory'); ?>/images/phone-icon.png">
	 					<h6><?=$content?></h6>

	 				</div>
	 				<div class="contactBlock">
                        <iframe height="90%" width="90%" border="0" marginwidth="0" marginheight="0" src="https://www.mapquest.com/embed/us/ny/suffolk-county-282934689?center=40.91700599999999,-72.662142&zoom=12&maptype=undefined"></iframe>
	 						
	 				</div>
<div class="contactBlock">
	<div id="fb">		
	<?php if(is_active_sidebar('sidebar-1')){
							dynamic_sidebar('sidebar-1');
							}
							?>
						</div>
				</div>
				</div>

 				<div class="contactChecker">
 					<div class="contactCheckerButton">
 						<h2><?php echo do_shortcode("[links category_name=SymptomChecker]"); ?></h2>
 					</div>
 					<div class="contactCheckerButton">
 						<h2><?php echo do_shortcode("[links category_name=DosageChecker]"); ?></h2>
 					</div>	
 				</div>


 				<div id="sponsorBox">
 					<img src="<?php bloginfo('template_directory'); ?>/images/allied-logo.png">
 					<h6>Allied Physicians Group</h6>
 					<button><?php echo do_shortcode("[links category_name=Visit]"); ?></button>
 				</div>

 			</div>

<?php get_footer(); ?>
        
        
                <script>
        $(document).ready(function(){
  $('.feedback_scroller').slick({
  infinite: true,
        autoplay: true,
  autoplaySpeed: 5000,
       arrows: true,
      adaptiveHeight: true
  });
});
        </script>
